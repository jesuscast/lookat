//
//  QueueViewController.swift
//  lookat
//
//  Created by Macbook on 5/8/16.
//  Copyright © 2016 jesus castaneda. All rights reserved.
//

import UIKit
import Shimmer

extension UIColor {
    static var bg_blue: UIColor {
        return UIColor(red:0.84, green:0.88, blue:0.93, alpha:1.0)
        /* ##D7E1EC */
    }
    static var shimmer: UIColor {
        return UIColor(red:0.28, green:0.42, blue:0.56, alpha:1.0)
    }
}

class CustomButton: UIButton {
    override var highlighted: Bool {
        get {
            return super.highlighted
        }
        set {
            if newValue {
                backgroundColor = UIColor.bg_blue
            }
            else {
                backgroundColor = UIColor.whiteColor()
            }
            super.highlighted = newValue
        }
    }
}

class QueueViewController: UIViewController {
    
    var onTalkPressed: (Void -> Void)? = nil
    var talkButton = CustomButton.newAutoLayoutView()
    var logoView = UIImageView.newAutoLayoutView()
    
    var topView = UIView.newAutoLayoutView()
    var bottomView = UIView.newAutoLayoutView()
    
    let matchingView: FBShimmeringView = FBShimmeringView()
    let matchingLabel: UILabel = UILabel()
    
    override func loadView() {
        super.loadView()
        view = UIView()
        topView.addSubview(logoView)
        view.addSubview(topView)
        view.addSubview(bottomView)
    }
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        logoView.image = UIImage(named: "logo-pdf.pdf")
        
        talkButton.setTitle("Let me talk.", forState: .Normal)
        talkButton.layer.cornerRadius = 10;
        talkButton.layer.borderColor = UIColor(red:0.29, green:0.565, blue:0.886, alpha:1.0).CGColor
        talkButton.layer.borderWidth = 2
        talkButton.addTarget(self, action: "talkTouched:", forControlEvents: .TouchUpInside)
        talkButton.setTitleColor(UIColor(red:0.482, green:0.471, blue:0.482, alpha:1), forState: .Normal)
        //talkButton.addTarget(self, action: "changeBtnColor:", forControlEvents: .TouchUpInside)
        //talkButton.addTarget(self, action: "changeBtnColorNormal:", forControlEvents: .TouchUpOutside)
        talkButton.setTitleColor(UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), forState: .Highlighted)
        talkButton.titleLabel?.font = UIFont (name: "Helvetica Neue", size: 30)
        topView.backgroundColor = UIColor.bg_blue // livetalk-blue
        bottomView.backgroundColor = UIColor.bg_blue
        
        
        // set the matching label styles
        matchingLabel.text = "Matching..."
        matchingLabel.textAlignment = NSTextAlignment.Center
        matchingLabel.textColor = UIColor.shimmer
        matchingLabel.font = UIFont(name: "AvenirNext-Regular", size: 30)
        matchingView.contentView = matchingLabel
        matchingView.shimmering = true
        matchingView.shimmeringBeginFadeDuration = 0.01
        matchingView.shimmeringOpacity = 0.5;
        matchingView.shimmeringPauseDuration = 0.01
        matchingView.shimmeringEndFadeDuration = 0.01
        matchingView.shimmeringAnimationOpacity = 1.0
        matchingView.shimmeringSpeed = 50
        NSLog("Shimerring length \(matchingView.shimmeringHighlightLength) ")
        bottomView.addSubview(matchingView)
        view.setNeedsUpdateConstraints()
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.matchingLabel.alpha = 1.0
    }
    
    // MARK: Actions
    @IBAction func talkTouched(sender: UIButton) -> Void {
        onTalkPressed?()
    }
    
    // MARK: AutoLayout
    var didSetupConstraints = false
    
    override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
        NSLog("Device flipped")
    }
    override func shouldAutorotate() -> Bool {
        return false
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.Portrait
    }
    
    override func updateViewConstraints() {
        if !didSetupConstraints {
            NSLog("sessionDidConnect")
            didSetupConstraints = true
            
            // Put the top view half the size of the screen on top
            topView.autoMatchDimension(.Height, toDimension: .Height, ofView: topView.superview!, withMultiplier: CGFloat(0.5))
            topView.autoMatchDimension(.Width, toDimension: .Width, ofView: topView.superview!, withMultiplier: CGFloat(1.0))
            topView.autoConstrainAttribute(.FirstBaseline, toAttribute: .FirstBaseline, ofView: topView.superview!)
            topView.autoAlignAxisToSuperviewAxis(.Vertical)
            
            // Put the bottom view half the size of the screen on the bottom
            bottomView.autoMatchDimension(.Height, toDimension: .Height, ofView: bottomView.superview!, withMultiplier: CGFloat(0.5))
            bottomView.autoMatchDimension(.Width, toDimension: .Width, ofView: bottomView.superview!, withMultiplier: CGFloat(1.0))
            bottomView.autoConstrainAttribute(.Bottom, toAttribute: .Bottom, ofView: bottomView.superview!)
            bottomView.autoAlignAxisToSuperviewAxis(.Vertical)
            
            // Put the logo in the middle of the top view
            let screenRect: CGRect = UIScreen.mainScreen().bounds
            let screenWidth: CGFloat = screenRect.size.width
            let screenHeight: CGFloat = screenRect.size.height
            
            let hP = Float(screenHeight)*0.5
            let wP = Float(screenWidth)
            let w1: Float = 200.0
            let h1: Float = 200.0
            let w2 = wP*0.3
            let h2 = w2*h1/w1
            let hM = Float(h2/hP)
            let wM = Float(w2/wP)
            logoView.autoAlignAxisToSuperviewAxis(.Horizontal)
            logoView.autoAlignAxisToSuperviewAxis(.Vertical)
            logoView.autoMatchDimension(.Height, toDimension: .Height, ofView: logoView.superview!, withMultiplier: CGFloat(hM))
            logoView.autoMatchDimension(.Width, toDimension: .Width, ofView: logoView.superview!, withMultiplier: CGFloat(wM))
            
            // Center the mathing view inside the bottomview
            let xTemp: CGFloat = self.matchingLabel.intrinsicContentSize().width
            let ratioTemp: CGFloat = xTemp / screenWidth
            let ratioViews: Float = ceilf(Float(ratioTemp)*100) / 100
            matchingView.autoMatchDimension(.Width, toDimension: .Width, ofView: matchingView.superview!, withMultiplier: CGFloat(ratioViews))
            matchingView.autoMatchDimension(.Height, toDimension: .Height, ofView: matchingView.superview!)
            matchingView.autoAlignAxisToSuperviewAxis(.Vertical)
            matchingView.autoAlignAxisToSuperviewAxis(.Horizontal)
            // Label
            matchingLabel.autoMatchDimension(.Height, toDimension: .Height, ofView: matchingLabel.superview!)
            matchingLabel.autoAlignAxisToSuperviewAxis(.Vertical)
            matchingLabel.autoAlignAxisToSuperviewAxis(.Horizontal)
        }
        
        super.updateViewConstraints()
    }
}
