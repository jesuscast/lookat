//
//  TWCInvite.h
//  TwilioConversationsClient
//
//  Copyright © 2016 Twilio Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TWCInvite.h"

@class TWCLocalMedia;
@class TWCConversation;
@class TWCIceOptions;

/**
 *  `TWCIncomingInvite` represents a request to join a Conversation.
 */
@interface TWCIncomingInvite : NSObject

/**
 *  The unique server identifier of the Conversation you are invited to.
 *  @discussion This value can be very useful for debugging purposes.
 */
@property (nonatomic, copy, readonly, nullable) NSString *conversationSid;

/**
 *  The identity of the inviter.
 */
@property (nonatomic, copy, readonly, nonnull) NSString *from;

/**
 *  The participants who have been invited to, or have already joined the Conversation.
 */
@property (nonatomic, copy, readonly, nonnull) NSArray *participants;

/**
 *  The current status of the Invite.
 *
 *  @discussion When a `TWCIncomingInvite` is raised to your `TwilioConversationClientDelegate` 
 *  its initial status will be `TWCInviteStatusPending`.
 */
@property (nonatomic, assign, readonly) TWCInviteStatus status;

/**
 *  Accepts an invite with the default local media configuration.
 *  @note The default media configuration shares your microphone, but not the camera.
 *
 *  @param acceptHandler The completion handler to be called when invite acceptance finishes.
 */
- (void)acceptWithCompletion:(nonnull TWCInviteAcceptanceBlock)acceptHandler;

/**
 *  Accepts an invite with a specific local media configuration.
 *
 *  @param localMedia    The local media object to share with participants.
 *  @param acceptHandler The completion handler to be called when invite acceptance finishes.
 */
- (void)acceptWithLocalMedia:(nonnull TWCLocalMedia *)localMedia
                  completion:(nonnull TWCInviteAcceptanceBlock)acceptHandler;

/**
 *  Accepts an invite with a specific local media configuration and custom ICE options.
 *
 *  @param localMedia    The local media object to share with participants.
 *  @param iceOptions    The custom ICE configuration to use.
 *  @param acceptHandler The completion handler to be called when invite acceptance finishes.
 */
- (void)acceptWithLocalMedia:(nonnull TWCLocalMedia *)localMedia
                  iceOptions:(nonnull TWCIceOptions *)iceOptions
                  completion:(nonnull TWCInviteAcceptanceBlock)acceptHandler;

/**
 *  Reject this invitation, sending a message indicating rejection to the requestor.
 */
- (void)reject;

@end
