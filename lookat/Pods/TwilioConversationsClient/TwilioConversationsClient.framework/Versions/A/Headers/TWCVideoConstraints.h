//
//  TWCVideoConstraints.h
//  TwilioConversationsClient
//
//  Copyright © 2016 Twilio Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <CoreMedia/CoreMedia.h>

/**
 *  The maximum fps allowed in your constraints. Limited to 30 fps at the moment.
 */
extern NSUInteger const TWCVideoConstraintsMaximumFPS;

/**
 *  The minimum fps allowed in your constraints.
 */
extern NSUInteger const TWCVideoConstraintsMinimumFPS;

/**
 *  Represents no video size constraint.
 *
 *  @discussion This constant can be passed as `maxSize` or `minSize` with
 *  `constraintsWithMaxSize:minSize:maxFrameRate:minFrameRate` when you don't want to set the max or min size
 *  video constraint.
 */
extern CMVideoDimensions const TWCVideoSizeConstraintsNone;

/**
 *  Represents no video frame rate constraint.
 *
 *  @discussion This constant can be passed as `maxFrameRate` or `minFrameRate` with
 *  `constraintsWithMaxSize:minSize:maxFrameRate:minFrameRate` when you don't want to set the max or min frame
 *  rate video constraint.
 */
extern NSUInteger const TWCVideoFrameRateConstraintsNone;

/**
 *  `TWCVideoConstraints` specifies requirements for local video capture.
 *
 *  @discussion Use this class in conjunction with `TWCLocalVideoTrack` to customize video capture for your use case.
 *  See `TWCCameraCapturer.h` for size and frame rate presets which pair well with that capturer.
 *  Note that `TWCVideoConstraints` is used to resolve the capture format, but the actual video sent to Participants
 *  may be downscaled temporally or spatially in response to network and device conditions.
 */
@interface TWCVideoConstraints : NSObject

/**
 *  The default video constraints (640x480x30).
 *
 *  @return Video constraints.
 */
+ (null_unspecified instancetype)constraints;

/**
 *  Constraints with a user provided minimum size and default max frame rate (30fps).
 *  @discussion This method is useful when you want to require a minimum video resolution (such as for HD calling).
 *
 *  @param minSize The minimum video size that is acceptable for capture.
 *
 *  @return Video constraints.
 */
+ (null_unspecified instancetype)constraintsWithMinSize:(CMVideoDimensions)minSize;

/**
 *  Constraints with a user provided maximum size and the default max frame rate (30fps).
 *
 *  @discussion This method is useful when you want to require a maximum video resolution (such as for sub-SD calling).
 *
 *  @param maxSize The maximum video size that is acceptable for capture.
 *
 *  @return Video constraints.
 */
+ (null_unspecified instancetype)constraintsWithMaxSize:(CMVideoDimensions)maxSize;

/**
 *  Constraints with user provided size and frame rate ranges.
 *  @discussion This advanced method allows you to select from within a range of acceptable values.
 *
 *  @param maxSize      The maximum video size you will accept.
 *  @param minSize      The minimum video size you will accept.
 *  @param maxFrameRate The maximum video frame rate you will accept.
 *  @param minFrameRate The minimum video frame rate you will accept.
 *
 *  @return Video constraints.
 */
+ (null_unspecified instancetype)constraintsWithMaxSize:(CMVideoDimensions)maxSize
                                                minSize:(CMVideoDimensions)minSize
                                           maxFrameRate:(NSUInteger)maxFrameRate
                                           minFrameRate:(NSUInteger)minFrameRate;

/**
 *  Specifies the maximum size for your video in native input coordinates (think landscape for `TWCCameraCapturer`).
 *
 *  @discussion Each dimension must be divisible by 8.
 */
@property (nonatomic, assign, readonly) CMVideoDimensions maxSize;

/**
 *  Specifies the minimum size for your video in native input coordinates.
 *
 *  @discussion Defaults to {0,0}, which indicates no minimum video size. 
 *  Set this property if you wish to choose a size within the range of `minSize` and `maxSize`.
 */
@property (nonatomic, assign, readonly) CMVideoDimensions minSize;

/**
 *  Specifies the maximum frame rate of your video (frames per second).
 *
 *  @discussion Frame rates are capped at `TWCVideoConstraintsMaximumFPS`.
 */
@property (nonatomic, assign, readonly) NSUInteger maxFrameRate;

/**
 *  Specifies the minimum frame rate of your video (frames per second).
 *
 *  @discussion Defaults to no minimum value.
 */
@property (nonatomic, assign, readonly) NSUInteger minFrameRate;

@end
