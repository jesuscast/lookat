//
//  TWCParticipant.h
//  TwilioConversationsClient
//
//  Copyright © 2016 Twilio Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TWCMedia.h"

@class TWCConversation;
@class TWCParticipant;

/**
 *  `TWCParticipantDelegate` provides updates as the status of `TWCParticipant` changes.
 */
@protocol TWCParticipantDelegate <NSObject>

@optional

/** Called when a participant adds a video track to their media.

 @param participant The participant.
 @param videoTrack The added video track.
 */
- (void)participant:(nonnull TWCParticipant *)participant addedVideoTrack:(nonnull TWCVideoTrack *)videoTrack;

/** Called when a participant removes a video track from their media.

 @param participant The participant.
 @param videoTrack The removed video track.
 */
- (void)participant:(nonnull TWCParticipant *)participant removedVideoTrack:(nonnull TWCVideoTrack *)videoTrack;

/** Called when a participant adds an audio track to their media.

 @param participant The participant.
 @param audioTrack The added audio track.
 */
- (void)participant:(nonnull TWCParticipant *)participant addedAudioTrack:(nonnull TWCAudioTrack *)audioTrack;

/** Called when a participant removes an audio track from their media.

 @param participant The participant.
 @param audioTrack The removed audio track.
 */
- (void)participant:(nonnull TWCParticipant *)participant removedAudioTrack:(nonnull TWCAudioTrack *)audioTrack;

/**
 *  Called when a participant's media track is disabled.
 *
 *  @param participant  The participant.
 *  @param track   The track which was disabled.
 */
- (void)participant:(nonnull TWCParticipant *)participant disabledTrack:(nonnull TWCMediaTrack *)track;

/**
 *  Called when a participant's media track is enabled.
 *
 *  @param participant  The participant.
 *  @param track   The track which was enabled.
 */
- (void)participant:(nonnull TWCParticipant *)participant enabledTrack:(nonnull TWCMediaTrack *)track;

@end

/**
 *  `TWCParticipant` represents a remote Client present in a Conversation.
 */
@interface TWCParticipant : NSObject

/** Callee identity. */
@property (nonatomic, readonly, strong, nonnull) NSString *identity;

/** Reference to conversation this participant is in. */
@property (nonatomic, readonly, weak, null_unspecified) TWCConversation *conversation;

@property (nonatomic, weak, nullable) id<TWCParticipantDelegate> delegate;

/**
 *  The media for this participant.
 */
@property (nonatomic, readonly, strong, nonnull) TWCMedia *media;

/** 
 * Participant SID.
 * @note sid is available when conversation gets connected (didConnectParticipant) 
 */
@property (nonatomic, readonly, strong, nullable) NSString *sid;

@end
