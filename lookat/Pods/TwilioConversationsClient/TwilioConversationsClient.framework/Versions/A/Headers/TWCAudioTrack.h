//
//  TWCAudioTrack.h
//  TwilioConversationsClient
//
//  Copyright © 2016 Twilio Inc. All rights reserved.
//

#import "TWCMediaTrack.h"

@interface TWCAudioTrack : TWCMediaTrack

@end
