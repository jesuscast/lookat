//
//  TWCInvite.h
//  TwilioConversationsClient
//
//  Copyright © 2016 Twilio Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TWCConversation;

/**
 *  Represents the current status of an Invite.
 */
typedef NS_ENUM(NSUInteger, TWCInviteStatus)
{
    /**
     *  The Invite has just been sent or received.
     */
    TWCInviteStatusPending = 0,

    /**
     *  The Invite is being accepted.
     */
    TWCInviteStatusAccepting,

    /**
     *  The Invite was accepted successfully, and resulted in a Conversation.
     */
    TWCInviteStatusAccepted,

    /**
     *  The Invite was rejected by the local Client.
     */
    TWCInviteStatusRejected,

    /**
     *  The Invite was cancelled by the sender.
     */
    TWCInviteStatusCancelled,

    /**
     *  The Invite was accepted, but a Conversation could not be created.
     */
    TWCInviteStatusFailed
};

/**
 *  A handler which is fired when Conversation creation completes.
 *
 *  @param conversation The conversation which was created. Populated on success.
 *  @param error        An error describing why the conversation was not created. Populated on failure.
 */
typedef void (^TWCInviteAcceptanceBlock)(TWCConversation * _Nullable conversation, NSError * _Nullable error);