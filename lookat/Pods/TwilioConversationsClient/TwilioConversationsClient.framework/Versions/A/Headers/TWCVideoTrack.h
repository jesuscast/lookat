//
//  TWCVideoTrack.h
//  TwilioConversationsClient
//
//  Copyright © 2016 Twilio Inc. All rights reserved.
//

#import "TWCMediaTrack.h"

#import "TWCVideoCapturer.h"
#import "TWCVideoRenderer.h"

@class TWCVideoTrack;

/**
 *  `TWCVideoTrackDelegate` allows you to respond to changes in `TWCVideoTrack`.
 *  @discussion This delegate is useful when paired with the `attach:` APIs.
 */
@protocol TWCVideoTrackDelegate <NSObject>

@optional

/**
 *  Fired every time the track's dimensions change.
 *  @discussion If you have attached views, and wish to resize your containers now would be a good time.
 *
 *  @param track      The track which was updated.
 *  @param dimensions The new dimensions of the video track.
 */
- (void)videoTrack:(nonnull TWCVideoTrack *)track dimensionsDidChange:(CMVideoDimensions)dimensions;

@end

/**
 *  `TWCVideoTrack` represents video, and provides an interface to render frames from the track.
 */
@interface TWCVideoTrack : TWCMediaTrack

/**
 *  The video track's delegate. Set the delegate to receive updates about the track.
 */
@property (nonatomic, weak, nullable) id<TWCVideoTrackDelegate> delegate;

/**
 *  An array of views that are currently attached to the video track.
 *  @note Use the 'attach:' and 'detach:' methods to manipulate this collection.
 */
@property (nonatomic, strong, readonly, nonnull) NSArray<UIView *> *attachedViews;

/**
 *  An array of renderers that are currently attached to the video track.
 *  @note Use the 'addRenderer:' and 'removeRenderer:' methods to manipulate this collection.
 */
@property (nonatomic, strong, readonly, nonnull) NSArray<id<TWCVideoRenderer>> *renderers;

/**
 *  The dimensions of the track's video. Use this to layout attached views.
 */
@property (nonatomic, assign, readonly) CMVideoDimensions videoDimensions;

/**
 *  Attaches a view to the video track. The track's contents will be drawn into the attached view.
 *  @discussion The `attach:` API is the simplest way to display video. For more control see `addRenderer:`.
 *
 *  @param view The view to attach.
 */
- (void)attach:(nonnull UIView *)view;

/**
 *  Detaches a view from the video track. The track's contents will no longer be drawn into the attached view.
 *
 *  @param view The view to detach.
 */
- (void)detach:(nonnull UIView *)view;

/**
 *  Adds a renderer to the video track. Renderers provide fine control over how video is displayed.
 *  @discussion Use this method instead of `attach` to add your own renderer to `TWCVideoTrack`.
 *
 *  @param renderer An object or swift struct that implements the `TWCVideoRenderer` protocol.
 */
- (void)addRenderer:(nonnull id<TWCVideoRenderer>)renderer;

/**
 *  Removes a renderer from the video track.
 *
 *  @param renderer An object or swift struct that implements the `TWCVideoRenderer` protocol.
 */
- (void)removeRenderer:(nonnull id<TWCVideoRenderer>)renderer;

@end

@class TWCVideoConstraints;

/**
 *  `TWCLocalVideoTrack` represents local video produced by a Capturer.
 */
@interface TWCLocalVideoTrack : TWCVideoTrack

/**
 *  Creates a video track from a capturer.
 *  @discussion The track will be created with default video constraints (640x480x30).
 *
 *  @param capturer A capturer which is bound to the track.
 *
 *  @return A video track ready to be added to the local media.
 */
- (nonnull instancetype)initWithCapturer:(nonnull id<TWCVideoCapturer>)capturer;

- (nonnull instancetype)initWithCapturer:(nonnull id<TWCVideoCapturer>)capturer
                             constraints:(nonnull TWCVideoConstraints *)constraints;

/**
 *  Enable or disable sharing video from the track. Defaults to 'YES'.
 */
@property (nonatomic, assign, getter=isEnabled) BOOL enabled;

/**
 *  The capturer that is assosciated with this track.
 */
@property (nonatomic, strong, readonly, nonnull) id<TWCVideoCapturer> capturer;

@property (nonatomic, strong, readonly, nonnull) TWCVideoConstraints *constraints;

@end
