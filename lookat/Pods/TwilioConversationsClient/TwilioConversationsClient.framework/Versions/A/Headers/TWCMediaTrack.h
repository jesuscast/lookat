//
//  TWCMediaTrack.h
//  TwilioConversationsClient
//
//  Copyright © 2016 Twilio Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, TWCMediaTrackState) {
    TWCMediaTrackStateIdle = 0,
    TWCMediaTrackStateStarting,
    TWCMediaTrackStateStarted,
    TWCMediaTrackStateEnding,
    TWCMediaTrackStateEnded
};

@interface TWCMediaTrack : NSObject

@property (nonatomic, assign, getter=isEnabled, readonly) BOOL enabled;
@property (nonatomic, assign, readonly) TWCMediaTrackState state;
@property (nonatomic, copy, readonly) NSString *trackId;


@end
