//
//  TWCCameraPreviewView.h
//  TwilioConversationsClient
//
//  Copyright © 2016 Twilio Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

/**
 *  `TWCCameraPreviewView` previews video captured by `TWCCameraCapturer`.
 *  @discussion Use this view to preview the camera feed before sharing it in a Conversation.
 */
@interface TWCCameraPreviewView : UIView

/**
 *  The current orientation of the preview view's content.
 */
@property (nonatomic, assign, readonly) UIInterfaceOrientation orientation;

@end
