//
//  TwilioConversationsClient.h
//  TwilioConversationsClient
//
//  Copyright © 2016 Twilio Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <TwilioCommon/TwilioAccessManager.h>

#import "TWCAudioTrack.h"
#import "TWCCameraCapturer.h"
#import "TWCCameraPreviewView.h"
#import "TWCClientOptions.h"
#import "TWCConversation.h"
#import "TWCInvite.h"
#import "TWCIncomingInvite.h"
#import "TWCOutgoingInvite.h"
#import "TWCIceOptions.h"
#import "TWCI420Frame.h"
#import "TWCMedia.h"
#import "TWCVideoConstraints.h"
#import "TWCVideoTrack.h"
#import "TWCVideoViewRenderer.h"
#import "TWCMediaTrackStatistics.h"

/*!
 *
 * Different logging levels.
 *
 */
typedef NS_ENUM(NSUInteger, TWCLogLevel) {
    TWCLogLevelOff = 0,      ///< The OFF level has the highest possible rank and is intended to turn off logging.
    TWCLogLevelFatal,        ///< The FATAL level designates very severe error events that will presumably lead the application to abort.
    TWCLogLevelError,        ///< The ERROR level designates error events that might still allow the application to continue running.
    TWCLogLevelWarning,      ///< The WARNING level designates potentially harmful situations.
    TWCLogLevelInfo,         ///< The INFO level designates informational messages that highlight the progress of the application at coarse-grained level.
    TWCLogLevelDebug,        ///< The DEBUG Level designates fine-grained informational events that are most useful to debug an application.
    TWCLogLevelTrace,        ///< The TRACE Level designates finer-grained informational events than the DEBUG
    TWCLogLevelAll           ///< The ALL level has the lowest possible rank and is intended to turn on all logging.
};

/*!
 *
 * Different logging modules.
 *
 */
typedef NS_ENUM(NSUInteger, TWCLogModule) {
    TWCLogModuleCore = 0,      ///< Conversations Core SDK
    TWCLogModulePlatform,      ///< Conversations iOS SDK
    TWCLogModuleSignaling,     ///< Signaling Module
    TWCLogModuleWebRTC         ///< WebRTC Module
};

/**
 *  TWCAudioOutput allows you to control audio playback and recording.
 */
typedef NS_ENUM(NSUInteger, TWCAudioOutput) {
    /**
     *  The default audio behaviour.
     *  Any audio route which supports both input and output will be selectable via control center.
     *  @note Airplay is not supported at the moment.
     */
    TWCAudioOutputDefault   = 0,
    /**
     *  Prefer the device loudspeaker and front facing microphone where possible, even when headphones are plugged in.
     */
    TWCAudioOutputSpeaker   = 1,
    /**
     *  Use the iPhone's built-in Receiver and bottom Microphone. On iPad and iPod Touch, optimize for audio only conversations.
     */
    TWCAudioOutputReceiver  = 2,
    /**
     *  Allow the application to manage the `AVAudioSession` as much as possible.
     *
     *  @discussion WebRTC will still try to configure the shared `AVAudioSession` each time recording starts.
     *  However, `TwilioConversationsClient` will make no further modifications to the `AVAudioSession` configuration.
     */
    TWCAudioOutputApplication = 3
};

@protocol TwilioConversationsClientDelegate;

/** This class declares the entity which represents the local conversationsClient. */
@interface TwilioConversationsClient : NSObject

/** ConversationsClient delegate. Set this property to be notified on action completion. */
@property (nonatomic, weak, nullable) id<TwilioConversationsClientDelegate> delegate;

/** Access manager to use with this client. */
@property (nonatomic, readonly, strong, nonnull) TwilioAccessManager *accessManager;

/** Identity of this conversationsClient on the network for incoming calls. */
@property (nonatomic, readonly, strong, nullable) NSString *identity;

/** Reflects current listening state of the conversationsClient. */
@property (nonatomic, assign, readonly) BOOL listening;

/*!
 *
 * @discussion This class method creates a TwilioConversationsClient using a TwilioAccessManager and a delegate. Delegate callbacks will be performed on the main queue.
 * @param accessManager - Instance of TwilioAccessManager.
 * @param delegate - A delegate conforming to TwilioConversationsClientDelegate for handling conversationsClient related events.
 * @return Reference of type TwilioConversationsClient.
 *
 */
+ (nullable TwilioConversationsClient *)conversationsClientWithAccessManager:(nonnull TwilioAccessManager *)accessManager
                                                                    delegate:(nullable id<TwilioConversationsClientDelegate>)delegate;

/* @discussion This class method creates a TwilioConversationsClient using a TwilioAccessManager, an options Dictionary, a dispatch queue and a delegate.
 * @param accessManager - Instance of TwilioAccessManager.
 * @param options - Custom client options which you would like to use.
 * @param queue - A serial dispatch queue where delegate callbacks will be received. If nil is provided, delegate calls will be performed on main queue.
 * @param delegate - A delegate conforming to the TwilioConversationsClientDelegate protocol for handling Client events.
 * @return Reference of type TwilioConversationsClient.
 *
 */
+ (nullable TwilioConversationsClient *)conversationsClientWithAccessManager:(nonnull TwilioAccessManager *)accessManager
                                                                     options:(nullable TWCClientOptions *)options
                                                               delegateQueue:(nullable dispatch_queue_t)queue
                                                                    delegate:(nullable id<TwilioConversationsClientDelegate>)delegate;

/** Register TwilioConversationsClient within Twilio infrastructure.
 
 This method will result in one of the following delegate methods being called:
 
    - (void)conversationsClientDidStartListeningForInvites:(TwilioConversationsClient *)conversationsClient;

    - (void)conversationsClient:(TwilioConversationsClient *)conversationsClient didFailToStartListeningWithError:(NSError *)error;
 
 If another party starts a conversation with your conversationsClient, you will receive the delegate call:
 
    - (void)conversationsClient:(TwilioConversationsClient *)conversationsClient didReceiveInvite:(TWCInvite *)invite;

 @see delegate
 @see unlisten
 */
- (void)listen;

/** Remove conversationsClient registration within Twilio infrastructure.
 
 This method may result in the following delegate methods being called:
 
 - (void)conversationsClientDidStopListeningForInvites:(TwilioConversationsClient *)conversationsClient error:(NSError *)error;
 
 @see listen
 */
- (void)unlisten;

/**
 *  Invites a single client to a new conversation.
 *
 *  @param client  The client identity to invite.
 *  @param handler A handler to be called when the invite finishes.
 *
 *  @note The default local media configuration is used (publish audio, but not video).
 *
 *  @return A TWCOutgoingInvite object which can be used to cancel the request.
 */
- (nullable TWCOutgoingInvite *)inviteToConversation:(nonnull NSString *)client
                                             handler:(nonnull TWCInviteAcceptanceBlock)handler;

/**
 *  Invites a single client to a new conversation.
 *
 *  @param client       The client identity to invite.
 *  @param localMedia   The local media you would like to share with clients.
 *  @param handler      A handler to be called when the invite finishes.
 *
 *  @return A TWCOutgoingInvite object which can be used to cancel the request.
 */
- (nullable TWCOutgoingInvite *)inviteToConversation:(nonnull NSString *)client
                                          localMedia:(nonnull TWCLocalMedia *)localMedia
                                             handler:(nonnull TWCInviteAcceptanceBlock)handler;

/**
 *  Invites one or more clients to a new conversation.
 *
 *  @param clients      The clients to invite. An array of NSStrings.
 *  @param localMedia   The local media you would like to share with clients.
 *  @param handler      A handler to be called when the invite finishes.
 *
 *  @return A TWCOutgoingInvite object which can be used to cancel the request.
 */
- (nullable TWCOutgoingInvite *)inviteManyToConversation:(nonnull NSArray<NSString *> *)clients
                                              localMedia:(nonnull TWCLocalMedia *)localMedia
                                                 handler:(nonnull TWCInviteAcceptanceBlock)handler;

/**
 *  Invites one or more clients to a new conversation.
 *
 *  @param clients      The clients to invite. An array of NSStrings.
 *  @param localMedia   The local media you would like to share with clients.
 *  @param iceOptions   The custom ICE configuration you would like to use.
 *  @param handler      A handler to be called when the invite finishes.
 *
 *  @return A TWCOutgoingInvite object which can be used to cancel the request.
 */
- (nullable TWCOutgoingInvite *)inviteManyToConversation:(nonnull NSArray<NSString *> *)clients
                                              localMedia:(nonnull TWCLocalMedia *)localMedia
                                              iceOptions:(nonnull TWCIceOptions *)iceOptions
                                                 handler:(nonnull TWCInviteAcceptanceBlock)handler;

/*!
 *
 * This returns the SDK version as a string.
 *
 */
+ (nonnull NSString *)version;

/*!
 *
 * Retrieve the log level in Twilio SDK.
 *
 * @return Current TWCLogLevel
 *
 */
+ (TWCLogLevel)logLevel;

/*!
 *
 * Set the log level in Twilio SDK. Default log level is Error.
 *
 * @param logLevel The TWCLogLevel in use by the system.
 *
 */
+ (void)setLogLevel:(TWCLogLevel)logLevel;

/*!
 *
 * Set the log level for a specific module in Twilio SDK.
 *
 * @param logLevel The TWCLogLevel to be used by the module.
 * @param module The TWCLogModule for which the log level needs to be set.
 *
 */
+ (void)setLogLevel:(TWCLogLevel)logLevel module:(TWCLogModule)module;

/*!
 *
 * Change the audio output for your device.
 * @discussion Use this is if you want to customize how Conversations use the audio subsystem.
 *
 * @param audioOutput The TWCAudioOutput in use by the system.
 *
 */
+ (void)setAudioOutput:(TWCAudioOutput)audioOutput;

/**
 *  Get the current audio output for your device.
 *
 *  @return The currently selected output. Defaults to `TWCAudioOutputDefault`.
 */
+ (TWCAudioOutput)audioOutput;

@end


/** This protocol declares the TwilioConversationsClient delegate methods. */
@protocol TwilioConversationsClientDelegate <NSObject>
@optional
/** This method is invoked on successful completion of registration.  
 
 @param conversationsClient The TwilioConversationsClient which is now listening for invites.
 */
- (void)conversationsClientDidStartListeningForInvites:(nonnull TwilioConversationsClient *)conversationsClient;

/** This method is invoked on failure to complete registration.  
 
 @param conversationsClient The TwilioConversationsClient which failed to start listening.
 @param error The error encountered.
 */
- (void)conversationsClient:(nonnull TwilioConversationsClient *)conversationsClient didFailToStartListeningWithError:(nonnull NSError *)error;

/** This method is invoked on unregistration completion after calling unlisten. 
 
 @param conversationsClient The TwilioConversationsClient which stopped listening.
 @param error The error encountered.
 */
- (void)conversationsClientDidStopListeningForInvites:(nonnull TwilioConversationsClient *)conversationsClient error:(nullable NSError *)error;

/** This method is invoked when incoming call is received.  
 
 @param conversationsClient The TwilioConversationsClient which received the invite.
 @param invite The invite which was received.
 */
- (void)conversationsClient:(nonnull TwilioConversationsClient *)conversationsClient didReceiveInvite:(nonnull TWCIncomingInvite *)invite;

/**
 *  This method is invoked when an invite is cancelled by its sender. For the identity of the sender, query 'from' on TWCIncomingInvite.
 *
 *  @param conversationsClient The TwilioConversationsClient which received the invite message.
 *  @param invite              The invite which was cancelled.
 */
- (void)conversationsClient:(nonnull TwilioConversationsClient *)conversationsClient inviteDidCancel:(nonnull TWCIncomingInvite *)invite;

@end
