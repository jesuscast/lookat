//
//  TWCInvite.h
//  TwilioConversationsClient
//
//  Copyright © 2016 Twilio Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TWCInvite.h"

/**
 *  `TWCOutgoingInvite` represents a request for other clients to join a new Conversation.
 */
@interface TWCOutgoingInvite : NSObject

/**
 *  The current status of the Invite.
 */
@property (nonatomic, assign, readonly) TWCInviteStatus status;

/**
 *  The clients which were invited to join a Conversation.
 */
@property (nonatomic, copy, readonly, nonnull) NSArray<NSString *> *to;

/**
 *  Cancels the outgoing invite (If it has been accepted, terminate).
 */
- (void)cancel;

@end
