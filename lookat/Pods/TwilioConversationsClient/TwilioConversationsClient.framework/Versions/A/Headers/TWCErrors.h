//
//  TWCErrors.h
//  TwilioConversationsClient
//
//  Copyright © 2016 Twilio Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  The domain for Twilio Conversations Framework errors.
 */
extern NSString *const TWCConversationsErrorDomain;

/*!
 *
 * @enum Twilio Conversations Framework Error Codes
 * @abstract Constants used by NSError to indicate errors in TWCConversationsErrorDomain.
 *
 */
typedef NS_ENUM(NSInteger, TWCErrorCode)
{
    /**
     *  An unknown error.
     */
    TWCErrorCodeUnknown = -1,

    /**
     *  Authenticating your Client failed due to invalid auth credentials.
     */
    TWCErrorCodeInvalidAuthData = 100,
    
    /**
     *  The SIP account was invalid.
     */
    TWCErrorCodeInvalidSIPAccount = 102,
    
    /**
     *  There was an error during Client registration.
     */
    TWCErrorCodeClientRegistration = 103,
    
    /**
     *  The Conversation was invalid.
     */
    TWCErrorCodeInvalidConversation = 105,
    
    /**
     *  The Client you invited was not available.
     */
    TWCErrorCodeConversationParticipantNotAvailable = 106,
    
    /**
     *  The Client rejected your invitation.
     */
    TWCErrorCodeConversationRejected = 107,
    
    /**
     *  The Client was busy, and could not handle your invitation.
     */
    TWCErrorCodeConversationIgnored = 108,
    
    /**
     *  The Conversation failed to start.
     *  @note: Check the error's `localizedDescription` for more information about the failure.
     */
    TWCErrorCodeConversationFailed = 109,
    
    /**
     *  The Conversation was terminated due to an unforseen error.
     */
    TWCErrorCodeConversationTerminated = 110,
    
    /**
     *  Establishing a media connection with the remote peer failed.
     */
    TWCErrorCodePeerConnectFailed = 111,
    
    /**
     *  The remote Client address was invalid.
     */
    TWCErrorCodeInvalidParticipantAddresses = 112,

    /**
     *  The Client disconnected unexpectedly.
     */
    TWCErrorCodeClientDisconnected = 200,

    /**
     *  The Client could not reconnect.
     */
    TWCErrorCodeClientFailedToReconnect = 201,

    /**
     *  Too many active Conversations.
     */
    TWCErrorCodeTooManyActiveConversations = 202,

    /**
     *  Too many tracks were added to the local media.
     *  @note: The current maximum is one video track at a time.
     */
    TWCErrorCodeTooManyTracks = 300,
    
    /**
     *  An invalid video capturer was added to the local media.
     *  @note: At the moment, only TWCCameraCapturer is supported.
     */
    TWCErrorCodeInvalidVideoCapturer = 301,
    
    /**
     *  An attempt was made to add or remove an invalid video track.
     *  @note: Use a video track created by a TWCCameraCapturer.
     */
    TWCErrorCodeInvalidVideoTrack = 302,
    
    /**
     *  An attempt was made to add or remove a video track, but the underlying video subsystem failed.
     *  @note: Retry your request at a later time.
     */
    TWCErrorCodeVideoFailed = 303,
    
    /**
     *  An attempt was made to add or remove a track that is already being operated on.
     *  @note: Retry your request at a later time.
     */
    TWCErrorCodeTrackOperationInProgress = 304,

    /**
     *  Adding a track to `TWCLocalMedia` asynchronously failed.
     */
    TWCErrorCodeTrackAddFailed = 305,
};
