//
//  TWCMediaTrackStatistics.h
//  TwilioConversationsClient
//
//  Copyright © 2016 Twilio Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreMedia/CoreMedia.h>


@interface TWCMediaTrackStatsRecord : NSObject

/** Track Identifier. */
@property (nonatomic, readonly) NSString *trackId;

/**Total number of RTP packets lost for this SSRC. Calculated as defined in [RFC3550] section 6.4.1.*/
@property (nonatomic, readonly) NSUInteger packetsLost;

/** Direction of media Sending or Receiving. */
@property (nonatomic, readonly) NSString *direction;

/** Name of Codec used. */
@property (nonatomic, readonly) NSString *codecName;

/** ssrc */
@property (nonatomic, readonly) NSString *ssrc;

/** Particiapnt Id. */
@property (nonatomic, readonly) NSString *participantSID;

/** Timestamp */
@property (nonatomic, assign, readonly) CFTimeInterval timestamp;

@end


/**
 *  `TWCLocalVideoMediaStatsRecord` provides information about video being sent to another participant.
 */
@interface TWCLocalVideoMediaStatsRecord : TWCMediaTrackStatsRecord

/** Total number of bytes sent for this SSRC. Calculated as defined in [RFC3550] section 6.4.1. */
@property (nonatomic, readonly) NSUInteger bytesSent;

/** Total number of RTP packets sent for this SSRC. Calculated as defined in [RFC3550] section 6.4.1. */
@property (nonatomic, readonly) NSUInteger packetsSent;

/** Dimension of captured video frame. */
@property (nonatomic, readonly, assign) CMVideoDimensions captureDimensions;

/** Dimension of sent video frame. */
@property (nonatomic, readonly, assign) CMVideoDimensions sentDimensions;

/** Encoded Frame rate - frames per second. */
@property (nonatomic, readonly) NSUInteger frameRate;

/**  Round trip time - in mili seconds */
@property (nonatomic, readonly) NSUInteger roundTripTime;

@end


/**
 *  `TWCLocalAudioMediaStatsRecord` provides information about audio being sent to another participant.
 */
@interface TWCLocalAudioMediaStatsRecord : TWCMediaTrackStatsRecord

/** Total number of bytes received for this SSRC. Calculated as defined in [RFC3550] section 6.4.1. */
@property (nonatomic, readonly) NSUInteger bytesSent;

/** Total number of RTP packets received for this SSRC. Calculated as defined in [RFC3550] section 6.4.1. */
@property (nonatomic, readonly) NSUInteger packetsSent;

/** Audio input level. */
@property (nonatomic, readonly) NSUInteger audioInputLevel;

/** Jitter received. */
@property (nonatomic, readonly) NSUInteger jitterReceived;

/** Packet Jitter measured in seconds for this SSRC. Calculated as defined in [RFC3550] section 6.4.1. */
@property (nonatomic, readonly) NSUInteger jitter;

/**  Round trip time - in mili seconds. */
@property (nonatomic, readonly) NSUInteger roundTripTime;

@end


/**
 *  `TWCRemoteVideoMediaStatsRecord` provides information about video being received from another participant.
 */
@interface TWCRemoteVideoMediaStatsRecord : TWCMediaTrackStatsRecord

/** Total number of bytes received for this SSRC. Calculated as defined in [RFC3550] section 6.4.1. */
@property (nonatomic, readonly) NSUInteger bytesReceived;

/** Total number of RTP packets received for this SSRC. Calculated as defined in [RFC3550] section 6.4.1. */
@property (nonatomic, readonly) NSUInteger packetsReceived;

/** Video Frame Dimensions. */
@property (nonatomic, readonly, assign) CMVideoDimensions dimensions;

/** Frame rate. */
@property (nonatomic, readonly) NSUInteger frameRate;

/** Jitter Buffer - in mili seconds */
@property (nonatomic, readonly) NSUInteger jitterBuffer;

@end


/**
 *  `TWCRemoteAudioMediaStatsRecord` provides information about audio being received from another participant.
 */
@interface TWCRemoteAudioMediaStatsRecord : TWCMediaTrackStatsRecord
/** Total number of bytes received for this SSRC. Calculated as defined in [RFC3550] section 6.4.1. */
@property (nonatomic, readonly) NSUInteger bytesReceived;

/** Total number of RTP packets received for this SSRC. Calculated as defined in [RFC3550] section 6.4.1. */
@property (nonatomic, readonly) NSUInteger packetsReceived;

/** Audio output level. */
@property (nonatomic, readonly) NSUInteger audioOutputLevel;

/** Jitter Buffer - in mili seconds. */
@property (nonatomic, readonly) NSUInteger jitterBuffer;

/** Jitter Received. */
@property (nonatomic, readonly) NSUInteger jitterReceived;

@end
