//
//  TWCVideoViewRenderer.h
//  TwilioConversationsClient
//
//  Copyright © 2016 Twilio Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TWCVideoRenderer.h"

@class TWCVideoViewRenderer;

/**
 *  `TWCVideoViewRendererDelegate` allows you to respond to, and customize the behaviour of `TWCVideoViewRenderer`.
 */
@protocol TWCVideoViewRendererDelegate <NSObject>

@optional

/**
 *  This method is called once, and only once after the first frame is received.
 *  Use it to drive user interface animations.
 *  @note: Querying hasVideoData will return 'YES' within, and after this call.
 *
 *  @param renderer The renderer which became ready.
 */
- (void)rendererDidReceiveVideoData:(nonnull TWCVideoViewRenderer *)renderer;

/**
 *  This method is called every time the video track's dimensions change.
 *
 *  @param renderer   The renderer.
 *  @param dimensions The new dimensions of the video stream.
 */
- (void)renderer:(nonnull TWCVideoViewRenderer *)renderer dimensionsDidChange:(CMVideoDimensions)dimensions;

/**
 *  This method is called every time the video track's orientation changes.
 *
 *  @param renderer     The renderer.
 *  @param orientation  The new orientation of the video stream.
 */
- (void)renderer:(nonnull TWCVideoViewRenderer *)renderer orientationDidChange:(TWCVideoOrientation)orientation;

/**
 *  Specify if the renderer or the application will handle rotated video content.
 *  @discussion Handling rotations at the application level is more complex, but allows you to smoothly animate transitions.
 *
 *  @param renderer The renderer.
 *  @return `NO` if you wish to handle rotations in your own layout. Defaults to `YES`.
 */
- (BOOL)rendererShouldRotateContent:(nonnull TWCVideoViewRenderer *)renderer;

@end

/**
 `TWCVideoViewRenderer` displays video inside a `UIView`.
 */
@interface TWCVideoViewRenderer : NSObject <TWCVideoRenderer>

/**
 *  Creates a video renderer with a delegate.
 *
 *  @param delegate An object implementing the TWCVideoViewRendererDelegate protocol (often a UIViewController).
 *
 *  @return A renderer which is appropriate for your device, and OS combination.
 */
- (nonnull instancetype)initWithDelegate:(nullable id<TWCVideoViewRendererDelegate>)delegate;

/**
 *  Creates a video renderer with a delegate.
 *
 *  @param delegate An object implementing the TWCVideoViewRendererDelegate protocol (often a UIViewController).
 *
 *  @return A renderer which is appropriate for your device, and OS combination.
 */
+ (nonnull TWCVideoViewRenderer *)rendererWithDelegate:(nullable id<TWCVideoViewRendererDelegate>)delegate;

/**
 *  A delegate which receives callbacks when important renderer events occur.
 *  @note The delegate is always called on the main thread in order to synchronize with UIKit.
 */
@property (nonatomic, weak, readonly, nullable) id<TWCVideoViewRendererDelegate> delegate;

/**
 *  The dimensions of incoming video frames (without rotations applied). Use this to layout the renderer's view.
 */
@property (nonatomic, assign, readonly) CMVideoDimensions videoFrameDimensions;

/**
 *  The orientation of incoming video frames. Use this to layout the renderer's view.
 */
@property (nonatomic, assign, readonly) TWCVideoOrientation videoFrameOrientation;

/**
 *  Indicates that at least one frame of video data has been received.
 */
@property (atomic, assign, readonly) BOOL hasVideoData;

/**
 *  The renderer's view. Add this to your view hierarchy to display rendered video content.
 */
@property (nonatomic, strong, readonly, nonnull) UIView *view;

@end
