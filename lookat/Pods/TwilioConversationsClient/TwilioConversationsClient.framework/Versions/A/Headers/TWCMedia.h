//
//  TWCMedia.h
//  TwilioConversationsClient
//
//  Copyright © 2016 Twilio Inc. All rights reserved.
//

#pragma mark -
#pragma mark - TWCMedia
#pragma mark -

@class TWCMediaTrack;
@class TWCAudioTrack;
@class TWCVideoTrack;
@class TWCCameraCapturer;

/**
 *  `TWCMedia` represents a collection of media tracks.
 */
@interface TWCMedia : NSObject

/**
 *  A collection of `TWCAudioTrack` objects.
 */
@property (nonatomic, strong, readonly, nonnull) NSArray<TWCAudioTrack *> *audioTracks;

/**
 *  A collection of `TWCVideoTrack` objects.
 */
@property (nonatomic, strong, readonly, nonnull) NSArray<TWCVideoTrack *> *videoTracks;

/**
 *  Find a track by its unique identifier.
 *
 *  @param trackId The unique identifier for the track.
 *
 *  @return A `TWCMediaTrack` if one can be found with the identifier provided, otherwise nil.
 */
- (nullable TWCMediaTrack *)getTrack:(nonnull NSString *)trackId;

@end

#pragma mark -
#pragma mark - TWCLocalMedia
#pragma mark -

@class TWCLocalMedia;

/**
 *  The `TWCLocalMediaDelegate` protocol allows you to respond to events from `TWCLocalMedia`.
 */
@protocol TWCLocalMediaDelegate <NSObject>

@optional

/**
 *  A video track was added to `TWCLocalMedia`.
 *  @discussion If you didn't already add a renderer when you created the track, now is a good time.
 *
 *  @param media      The `TWCLocalMedia` which has a new track.
 *  @param videoTrack The `TWCVideoTrack` which will be shared with Participants.
 */
- (void)localMedia:(nonnull TWCLocalMedia *)media didAddVideoTrack:(nonnull TWCVideoTrack *)videoTrack;

/**
 *  A video track failed to be added to `TWCLocalMedia`.
 *
 *  @discussion Asynchronous track errors may occur due to invalid `TWCVideoConstraints`, or other 
 *  failures in the video subsytem. Check the error that is returned for recovery strategies.
 *
 *  @param media      The `TWCLocalMedia` where the track was originally added.
 *  @param videoTrack The `TWCVideoTrack` which is no longer valid.
 *  @param error      An `NSError` describing why the track was not added.
 */
- (void)localMedia:(nonnull TWCLocalMedia *)media didFailToAddVideoTrack:(nonnull TWCVideoTrack *)videoTrack error:(nonnull NSError *)error;

/**
 *  A video track was removed from `TWCLocalMedia`.
 *  @discussion Renderers are automatically removed from the track after this call returns.
 *
 *  @param media      The `TWCLocalMdeia` where the track was removed.
 *  @param videoTrack The `TWCVideoTrack` which was removed.
 */
- (void)localMedia:(nonnull TWCLocalMedia *)media didRemoveVideoTrack:(nonnull TWCVideoTrack *)videoTrack;

@end

/**
 *  `TWCLocalMedia` represents the media sources that will be shared with other Conversation Participants.
 */
@interface TWCLocalMedia : TWCMedia

/**
 *  Indicates wether your local audio is muted.
 */
@property (nonatomic, assign, getter=isMicrophoneMuted) BOOL microphoneMuted;

/**
 *  Indicates if you are sharing audio.
 */
@property (nonatomic, assign, readonly, getter=isMicrophoneAdded) BOOL microphoneAdded;

/**
 *  An object conforming to the `TWCLocalMediaDelegate` protocol, which will respond to events from `TWCLocalMedia`.
 */
@property (nonatomic, weak, nullable) id<TWCLocalMediaDelegate> delegate;

/** Initializes a new TWCLocalMedia object.

 @param delegate A delegate which will receive local media callbacks.
 @return The new TWCLocalMedia object
 */
/**
 *  Initializes a new `TWCLocalMedia` object.
 *
 *  @param delegate A delegate which will receive local media callbacks.
 *
 *  @return A new `TWCLocalMedia` object if successful, otherwise `nil` in case of failures.
 */
- (nullable instancetype)initWithDelegate:(nullable id<TWCLocalMediaDelegate>)delegate;

/**
 *  Add a video track to the local media collection.
 *
 *  @param track The video track to add.
 */
- (BOOL)addTrack:(nonnull TWCVideoTrack *)track;

/**
 *  Add a video track to the local media collection.
 *
 *  @param track The video track to add.
 *  @param error An error if the track could not be added.
 *  @return 'YES' if success, 'NO' if failure.
 */
- (BOOL)addTrack:(nonnull TWCVideoTrack *)track error:(NSError * _Nullable * _Nullable)error;

/**
 *  Remove a video track from the local media collection.
 *
 *  @param track The video track to remove.
 */
- (BOOL)removeTrack:(nonnull TWCVideoTrack *)track;

/**
 *  Remove a video track from the local media collection.
 *
 *  @param track The video track to remove.
 *  @param error An error if the track could not be removed.
 *  @return 'YES' if success, 'NO' if failure.
 */
- (BOOL)removeTrack:(nonnull TWCVideoTrack *)track error:(NSError * _Nullable * _Nullable)error;

/**
 *  Creates a default camera capturer and adds its track to the local media collection.
 *
 *  @return The capturer which was added.
 */
- (nullable TWCCameraCapturer *)addCameraTrack;

/**
 *  Creates a default camera capturer and adds its track to the local media collection.
 *  @param error The error occuered while adding the camera track.
 *  @param error An error if the camera track could not be added.
 *  @return The capturer which was added.
 */
- (nullable TWCCameraCapturer *)addCameraTrack:(NSError * _Nullable * _Nullable)error;

/** Adds audio stream to local media, if not already added
 
 @return Result of operation, YES if successful, NO if microphone is already added or operation submitted last is in progress.
 */
- (BOOL)addMicrophone;

/** Removes audio stream from local media, if already added
 
 @return Result of operation, YES if successful, NO if microphone was not added or operation submitted last is in progress.
 */
- (BOOL)removeMicrophone;

@end
