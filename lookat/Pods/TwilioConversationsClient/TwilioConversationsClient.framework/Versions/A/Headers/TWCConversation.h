//
//  TWCConversation.h
//  TwilioConversationsClient
//
//  Copyright © 2016 Twilio Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TWCOutgoingInvite.h"
#import "TWCParticipant.h"
#import "TWCMedia.h"

@protocol TWCConversationDelegate;
@protocol TWCMediaTrackStatisticsDelegate;

#pragma mark - TWCConversation definition

/** Object that represents a current or pending conversation */
@interface TWCConversation : NSObject

/** An array of participants in the current conversation. */
@property (nonatomic, readonly, strong, nonnull) NSArray<TWCParticipant *> *participants;

/** Representation of the local video and audio stream */
@property (nonatomic, strong, readonly, nullable) TWCLocalMedia *localMedia;

/** The delegate for this conversation. */
@property (nonatomic, weak, nullable) id<TWCConversationDelegate> delegate;

/** The media track statistics delegate for this conversation. */
@property (nonatomic, weak, nullable) id<TWCMediaTrackStatisticsDelegate> statisticsDelegate;

/** This conversation's identifier */
@property (nonatomic, copy, readonly, nullable) NSString *sid;

/** Disconnect from this conversation.

 @return 'YES' if success, 'NO' if failure.
*/
- (BOOL)disconnect;

/** Disconnect from this conversation.
 
 @param error The error encountered
 @return 'YES' if disconnecected, 'NO' if error was encountered.
 */
- (BOOL)disconnect:(NSError * _Nullable * _Nullable)error;

/** Invite an additional client to the conversation.

 @param clientIdentity The client identity to invite.
 @param error The error encountered
 @return 'YES' if success, 'NO' if error was encountered.
 */
- (BOOL)invite:(nonnull NSString *)clientIdentity error:(NSError * _Nullable * _Nullable)error;

/** Invite additional clients to the conversation.
 
 @param clientIdentities The client identities.
 @param error The error encountered
 @return 'YES' if success, 'NO' if error was encountered.
 */
- (BOOL)inviteMany:(nonnull NSArray<NSString *> *)clientIdentities error:(NSError * _Nullable * _Nullable)error;

/** Get participant with participant SID.
 
 @param participantSID The Participant SID.
 @return participant with participant SID. Returns nil if participant not found.
 */
- (TWCParticipant * _Nullable)getParticipant:(nonnull NSString *)participantSID;

@end

@class TWCMediaTrackStatsRecord;

/** Delegate for TWCConversation objects.  This delegate is called with conversation life cycle events. */
@protocol TWCConversationDelegate <NSObject>
@optional

/** Called when participant is connected to conversation.
 
 @param conversation The conversation.
 @param participant The participant.
 */
- (void)conversation:(nonnull TWCConversation *)conversation didConnectParticipant:(nonnull TWCParticipant *)participant;

/** Called when participant is not connected due to an error.
 
 @param conversation The conversation.
 @param participant The participant.
 @param error The error encountered in adding the pariticpant to the conversation, if any.
 */
- (void)conversation:(nonnull TWCConversation *)conversation didFailToConnectParticipant:(nonnull TWCParticipant *)participant error:(nonnull NSError *)error;


/** Called when specified participant is disconnected from conversation either by request or due to an error. 
 
 @param conversation The Conversation which the Participant disconnected from.
 @param participant The Participant which disconnected.
 */
- (void)conversation:(nonnull TWCConversation *)conversation didDisconnectParticipant:(nonnull TWCParticipant *)participant;

/** Called when the conversation ends after the last participant leaves.
 
 @param conversation The Conversation that ended.
 */
- (void)conversationEnded:(nonnull TWCConversation *)conversation;

/** Called when the Conversation ends with an error.
 
 @param conversation The Conversation that ended.
 @param error An error indicating why the Conversation ended.
 */
- (void)conversationEnded:(nonnull TWCConversation *)conversation error:(nonnull NSError *)error;

@end

/** Media Track Statistics Delegate for TWCConversation. This delegate is called when media track statistics are received */
@protocol TWCMediaTrackStatisticsDelegate <NSObject>
@required
/** Called when statistics are received for a track.
 @param conversation The conversation.
 @param trackStatistics Track statistics record.
 */
- (void)conversation:(nonnull TWCConversation *)conversation didReceiveTrackStatistics:(nonnull TWCMediaTrackStatsRecord *)trackStatistics;

@end
