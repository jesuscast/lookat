//
//  TWCClientOptions.h
//  TwilioConversationsClient
//
//  Copyright © 2016 Twilio Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TWCIceOptions;

/**
 *  `TWCClientOptions` specifies custom settings to use with your Client.
 *  @discussion The options you provide persist to all Conversations started with this Client.
 */
@interface TWCClientOptions : NSObject

/**
 *  Custom ICE settings. See `TWCIceOptions.h` for more information.
 */
@property (nonatomic, strong, readonly, nonnull) TWCIceOptions *iceOptions;

/**
 *  Enable H.264 support (experimental).
 *  @discussion Defaults to `NO`. Both participants must support H.264 for the codec to be selected.
 *  While this option does take advantage of the hardware encoder/decoder - software format conversions reduce its performance.
 */
@property (nonatomic, assign, readonly) BOOL preferH264;

/**
 *  Creates `TWCClientOptions` with ICE settings.
 *
 *  @param options    The `TWCIceOptions` to use.
 *
 *  @return A `TWCClientOptions` object.
 */
- (null_unspecified instancetype)initWithIceOptions:(nonnull TWCIceOptions *)options;

/**
 *  Creates `TWCClientOptions` with ICE, and Codec settings.
 *
 *  @param options    The `TWCIceOptions` to use.
 *  @param preferH264 Prefer the H.264 codec over VP8.
 *
 *  @return A `TWCClientOptions` object.
 */
- (null_unspecified instancetype)initWithIceOptions:(nonnull TWCIceOptions *)options
                                         preferH264:(BOOL)preferH264;

@end
