//
//  TWCVideoCapturer.h
//  TwilioConversationsClient
//
//  Copyright © 2016 Twilio Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

/** Video Capture Source */
typedef NS_ENUM(NSUInteger, TWCVideoCaptureSource) {
    TWCVideoCaptureSourceFrontCamera = 0, ///< Front facing device camera
    TWCVideoCaptureSourceBackCamera       ///< Back facing device camera
};

@class TWCLocalVideoTrack;

@protocol TWCVideoCapturer <NSObject>

@property (atomic, assign, readonly, getter = isCapturing) BOOL capturing;

@property (nonatomic, weak, nullable) TWCLocalVideoTrack *videoTrack;

@end
