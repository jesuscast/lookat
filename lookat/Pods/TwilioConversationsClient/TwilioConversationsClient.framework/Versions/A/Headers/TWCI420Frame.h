//
//  TWCI420Frame.h
//  TwilioConversationsClient
//
//  Copyright © 2016 Twilio Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Specifies the orientation of video content.
 */
typedef NS_ENUM(NSUInteger, TWCVideoOrientation) {
    /**
     *  The video is rotated 0 degrees, oriented with its top side up.
     */
    TWCVideoOrientationUp = 0,
    /**
     *  The video is rotated 90 degrees, oriented with its top side to the left.
     */
    TWCVideoOrientationLeft,
    /**
     *  The video is rotated 180 degrees, oriented with its top side to bottom.
     */
    TWCVideoOrientationDown,
    /**
     *  The video is rotated 270 degrees, oriented with its top side to the right.
     */
    TWCVideoOrientationRight,
};

/**
 *  A helper which constructs an affine transform for any orientation.
 *
 *  @param orientation The orientation of the video you wish to display.
 *
 *  @return A `CGAffineTransform` struct which can be applied to a renderer's view.
 */
static inline CGAffineTransform TWCVideoOrientationMakeTransform(TWCVideoOrientation orientation)
{
    return CGAffineTransformMakeRotation((CGFloat)orientation * M_PI_2);
}

/**
 *  A helper which indicates if the orientation would cause the native dimensions to be flipped.
 *
 *  @param orientation The orientation to check.
 *
 *  @return `YES` if the orientation would cause the dimensions to be flipped, and `NO` otherwise.
 */
static inline BOOL TWCVideoOrientationIsRotated(TWCVideoOrientation orientation)
{
    return (orientation == TWCVideoOrientationLeft ||
            orientation == TWCVideoOrientationRight);
}

/**
 *  Represents a planar YUV video frame in the `I420` pixel format.
 */
@interface TWCI420Frame : NSObject

/**
 *  The native width of the frame, not accounting for any `orientation` metadata.
 */
@property(nonatomic, readonly) NSUInteger width;

/**
 *  The native height of the frame, not accounting for any `orientation` metadata.
 */
@property(nonatomic, readonly) NSUInteger height;

/**
 *  The width of the chroma (u,v) planes. Chroma information is horizontally subsampled at every 2nd pixel.
 */
@property(nonatomic, readonly) NSUInteger chromaWidth;

/**
 *  The height of the chroma (u,v) planes. Chroma information is vertically subsampled at every 2nd pixel.
 */
@property(nonatomic, readonly) NSUInteger chromaHeight;

/**
 *  The total size of a chroma plane in bytes, including padding.
 */
@property(nonatomic, readonly) NSUInteger chromaSize;

/**
 *  The orientation of the video frame. See `TWCVideoOrientation` for more details.
 */
@property(nonatomic, readonly) TWCVideoOrientation orientation;

/**
 *  A const pointer to the base of the y-plane (luma).
 */
@property(nonatomic, readonly) const uint8_t *yPlane;

/**
 *  A const pointer to the base of the u-plane (chroma).
 */
@property(nonatomic, readonly) const uint8_t *uPlane;

/**
 *  A const pointer to the base of the v-plane (chroma).
 */
@property(nonatomic, readonly) const uint8_t *vPlane;

/**
 *  The total width of each y-plane row including padding.
 */
@property(nonatomic, readonly) NSInteger yPitch;

/**
 *  The total width of each u-plane row including padding.
 */
@property(nonatomic, readonly) NSInteger uPitch;

/**
 *  The total width of each v-plane row including padding.
 */
@property(nonatomic, readonly) NSInteger vPitch;

@end

