//
//  TWCIceOptions.h
//  TwilioConversationsClient
//
//  Copyright © 2016 Twilio Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  `TWCIceTransportPolicy` specifies which ICE transports to allow.
 */
typedef NS_ENUM(NSUInteger, TWCIceTransportPolicy) {
    /**
     *  All transports will be used.
     */
    TWCIceTransportPolicyAll = 0,
    /**
     *  Only TURN relay transports will be used.
     */
    TWCIceTransportPolicyRelay = 1
};

/**
 *  `TWCIceServer` is a single STUN or TURN server.
 */
@interface TWCIceServer : NSObject

/**
 *  The URL for the ICE server.
 *  @discussion Your server URL must begin with either the stun: or turn: scheme.
 *  For example, a STUN server could be defined as <stun:stun.company.com:port>.
 */
@property (nonnull, nonatomic, readonly, copy) NSString *url;
@property (nullable, nonatomic, readonly, copy) NSString *username;
@property (nullable, nonatomic, readonly, copy) NSString *password;

/**
 *  Creates a `TWCIceServer`.
 *
 *  @param serverUrl The URL for your STUN or TURN server.
 *
 *  @return A `TWCIceServer` object.
 */
- (_Null_unspecified instancetype)initWithURL:(nonnull NSString *)serverUrl;

/**
 *  Creates a `TWCIceServer`.
 *
 *  @param serverUrl The URL for your STUN or TURN server.
 *  @param username  The username credential for your server.
 *  @param password  The password credential for your server.
 *
 *  @return A `TWCIceServer` object.
 */
- (_Null_unspecified instancetype)initWithURL:(nonnull NSString *)serverUrl
                                     username:(nullable NSString *)username
                                     password:(nullable NSString *)password;


@end

/**
 *  `TWCIceOptions` specifies custom media connectivity configurations.
 *
 *  @discussion Media connections are established using the ICE (Interactive Connectivity Establishment) protocol.
 *  These options allow you to customize how data flows to and from participants, and which protocols to use.
 *  You may also provide your own ICE servers, overriding the defaults.
 *  https://www.twilio.com/stun-turn
 */
@interface TWCIceOptions : NSObject

/**
 *  An array of `TWCIceServer` objects to be used during connection establishment.
 */
@property (nonatomic, copy, nonnull, readonly) NSArray<TWCIceServer *> *iceServers;

/**
 *  The transport policy to use. Defaults to `TWCIceTransportPolicyAll`.
 */
@property (nonatomic, assign, readonly) TWCIceTransportPolicy iceTransportPolicy;

/**
 *  Creates a `TWCIceOptions` instance with a custom transport policy.
 *
 *  @param transportPolicy The `TWCIceTransportPolicy` to use.
 *
 *  @return An initalized object if the policy was valid, otherwise nil.
 */
- (_Null_unspecified instancetype)initWithTransportPolicy:(TWCIceTransportPolicy)transportPolicy;

/**
 *  Creates a `TWCIceOptions` instance with a custom transport policy, and servers.
 *
 *  @discussion If you provide no servers then Twilio's Network Traversal Service will be used.
 *
 *  @param transportPolicy The `TWCIceTransportPolicy` to use.
 *  @param servers         An array of STUN and TURN servers in the form of `TWCIceServer` objects.
 *
 *  @return An initalized object if the policy was valid, otherwise nil.
 */
- (_Null_unspecified instancetype)initWithTransportPolicy:(TWCIceTransportPolicy)transportPolicy
                                                  servers:(nonnull NSArray<TWCIceServer *> *)servers;

@end
