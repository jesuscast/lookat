//
//  ViewController.swift
//  lookat
//
//  Created by Macbook on 5/7/16.
//  Copyright © 2016 jesus castaneda. All rights reserved.
//

import UIKit
import CoreLocation

var defaults = NSUserDefaults.standardUserDefaults()

// Necessary for location.

class ViewController: UIViewController, CLLocationManagerDelegate {
    //--------------------------------
    // MARK:  Instance Variables
    //--------------------------------
    // Location Variables.
    var locationManager: CLLocationManager?
    var lat : CLLocationDegrees? = nil
    var long : CLLocationDegrees? = nil
    // Size of the current screen, for position calculations and animations.
    var screenRect = CGRect(x: 0, y: 0, width: 0, height: 0)
    var screenWidth = CGFloat(0.0)
    var screenHeight = CGFloat(0.0)
    // Video SDK components
    var accessToken = "TWILIO_ACCESS_TOKEN"
    var accessManager: TwilioAccessManager?
    var client: TwilioConversationsClient?
    var localMedia: TWCLocalMedia?
    var camera: TWCCameraCapturer?
    var conversation: TWCConversation?
    var incomingInvite: TWCIncomingInvite?
    var outgoingInvite: TWCOutgoingInvite?
    var localMediaView : UIView = UIView()
    var remoteMediaView: UIView = UIView()
    var queue_view_controller : UIViewController = QueueViewController()
    var conversation_view_controller : ConversationViewController = ConversationViewController()
    // Socket Comminucation Variables
    let serverAddress: CFString = "73.231.186.204" //"127.0.0.1"//"104.236.141.41"
    let serverPort: UInt32 = 8124
    var inputStream: NSInputStream!
    var outputStream: NSOutputStream!
    // State Variables.
    var talking_now = false

    
    //--------------------------------
    // MARK:  Helper Functions
    //--------------------------------
    
    struct Platform {
        // Helper to determine if we're running on simulator or device
        static let isSimulator: Bool = {
            var isSim = false
            #if arch(i386) || arch(x86_64)
                isSim = true
            #endif
            return isSim
        }()
    }
    
    func checkForLocation(){
        /* Checks if I have acces to location */
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.requestWhenInUseAuthorization()
        checkLocationStatus()
    }
    func checkLocationStatus(){
        switch CLLocationManager.authorizationStatus() {
        case .AuthorizedAlways:
            print("I am always authorized with the location")
            locationManager?.startMonitoringSignificantLocationChanges()
            // Apparently I also need to actually obtain the location.
            locationManager?.requestLocation()
        case .AuthorizedWhenInUse:
            print("I am only authorized when in use")
            locationManager?.startMonitoringSignificantLocationChanges()
            // Apparently I also need to actually obtain the location.
            locationManager?.requestLocation()
        case .Denied:
            print("I am denied of location")
        case .NotDetermined:
            print("My location status has not been determined yet")
        case .Restricted:
            print("My location status is restricted")
        }
    }
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("The location did update")
        // I stop updating the location because at this point in time I am only able
        // to handle the initial location.
        locationManager?.stopMonitoringSignificantLocationChanges()
        if let locationManagerTmp = locationManager {
            lat = locationManagerTmp.location!.coordinate.latitude
            long = locationManagerTmp.location!.coordinate.longitude
            print("lat: \(lat!)")
            print("long: \(long!)")
            connect()
        } else {
            print("For some reason it says that I have authorized the loaction but it is not present")
        }
        locationManager = nil
    }
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        print("I feel like I have nothing to do here because it will be detected by didUpdateLocations")
        // Although I need ot start receiving the location.
        checkLocationStatus()
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Unfortunately I failed with error \(error)")
    }
    func createScreenVariables(){
        /* Fills the values of the screen size variables with their respective values. */
        screenRect = UIScreen.mainScreen().bounds
        screenWidth = screenRect.size.width
        screenHeight = screenRect.size.height
    }
    
    func initializeAllFrames(){
        queue_view_controller.view.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        conversation_view_controller.view.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        self.view.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        self.localMediaView.frame = CGRect(x: 0, y: 0, width: (screenWidth/3), height: (screenHeight/3))
        self.remoteMediaView.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        self.conversation_view_controller.onTimerExpired = onTimerExpired
        self.conversation_view_controller.onTalkPressed = onTalkPressed
    }
    
    func showQueueScreen() {
        print("Showing screen #1")
        // Set up the first view controller to the view controller
        self.view.addSubview(queue_view_controller.view)
    }
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        checkForLocation()
        createScreenVariables()
        initializeAllFrames()
        let screen = 1
        switch screen {
        case 1:
            showQueueScreen()
        default:
            print("no default value to show")

        }
    }
    
    // Once access token is set, initialize the Conversations SDK and display the identity of the
    // current user
    func initializeClient() {
        // Set up Twilio Conversations client
        self.accessManager = TwilioAccessManager(token:self.accessToken, delegate:self);
        self.client = TwilioConversationsClient(accessManager: self.accessManager!, delegate: self);
        self.client?.listen();
        
        self.startPreview()
        print("\(self.client?.identity!)")
    }
    func hangup() {
        print("disconnecting")
        self.conversation?.disconnect()
    }
    func updateName() {
        let message = ["content": "\(self.client!.identity!)", "type": "update_name"]
        sendMessage(message)
    }
    
    func conversationsClientDidStartListeningForInvites(conversationsClient: TwilioConversationsClient) {
        print("I am connected to twilio")
        print("\(self.client!.identity!)")
        joinChat()
        // updateName()
    }
    
    func startPreview() {
        // Setup local media preview
        self.localMedia = TWCLocalMedia(delegate: self)
        self.camera = self.localMedia?.addCameraTrack()
        
        if((self.camera) != nil && Platform.isSimulator != true) {
            self.camera!.videoTrack?.attach(self.localMediaView)
            self.camera!.videoTrack?.delegate = self;
            
            // Start the preview.
            self.camera!.startPreview();
            self.localMediaView.addSubview((self.camera!.previewView)!)
            self.camera!.previewView?.frame = self.localMediaView.bounds
            self.camera!.previewView?.contentMode = .ScaleAspectFit
            self.camera!.previewView?.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        }
    }
    func invite(client_identity : String) {
        if !(self.talking_now) {
            // I need to set it up right here in order to avoid conflicts.
            self.talking_now = true
            // self.conversation?.disconnect()
            // self.conversation = nil
            let invitee = client_identity
            self.outgoingInvite = self.client?.inviteToConversation(invitee, localMedia:self.localMedia!)
            { conversation, err in
                if err == nil {
                    conversation!.delegate = self
                    self.conversation = conversation
                    self.talking_now = true
                } else {
                    print("error creating conversation")
                    print(err)
                    self.talking_now = false
                }
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

// MARK: TWCLocalMediaDelegate
extension ViewController: TWCLocalMediaDelegate {
    func localMedia(media: TWCLocalMedia, didAddVideoTrack videoTrack: TWCVideoTrack) {
        print("added media track")
    }
}

// MARK: TWCVideoTrackDelegate
extension ViewController: TWCVideoTrackDelegate {
    func videoTrack(track: TWCVideoTrack, dimensionsDidChange dimensions: CMVideoDimensions) {
        print("video dimensions changed")
    }
}

// MARK: TwilioAccessManagerDelegate
extension ViewController: TwilioAccessManagerDelegate {
    func accessManagerTokenExpired(accessManager: TwilioAccessManager!) {
        print("access token has expired")
    }
    
    func accessManager(accessManager: TwilioAccessManager!, error: NSError!) {
        print("Access manager error:")
        print(error)
    }
}

// MARK: TwilioConversationsClientDelegate
extension ViewController: TwilioConversationsClientDelegate {
    func conversationsClient(conversationsClient: TwilioConversationsClient,
                             didFailToStartListeningWithError error: NSError) {
        print("failed to start listening:")
        print(error)
    }
    
    // Automatically accept any invitation
    func conversationsClient(conversationsClient: TwilioConversationsClient,
                             didReceiveInvite invite: TWCIncomingInvite) {
        dispatch_async(dispatch_get_main_queue()) {
            if !(self.talking_now) {
                // self.hangup()
                print(invite.from)
                invite.acceptWithLocalMedia(self.localMedia!) { conversation, error in
                    do {
                        self.conversation = try conversation
                        self.conversation!.delegate = try self
                    } catch {
                        print("Error")
                    }
                }
                self.talking_now = true
            }
        }
    }
}

// MARK: TWCConversationDelegate
extension ViewController: TWCConversationDelegate {
    func conversation(conversation: TWCConversation,
                      didConnectParticipant participant: TWCParticipant) {
        self.navigationItem.title = participant.identity
        participant.delegate = self
    }
    
    func conversation(conversation: TWCConversation,
                      didDisconnectParticipant participant: TWCParticipant) {
        self.navigationItem.title = "participant left"
        self.conversation_view_controller.didBothAccepted = false
        onTimerExpired()
    }
    
    func conversationEnded(conversation: TWCConversation) {
        self.navigationItem.title = "no call connected"
        
        // Restart the preview.
        self.startPreview()
    }
}

// MARK: TWCParticipantDelegate
extension ViewController: TWCParticipantDelegate {
    func participant(participant: TWCParticipant, addedVideoTrack videoTrack: TWCVideoTrack) {
        // self.remoteMediaView.frame = CGRect(0,0,0,0)
        videoTrack.attach(self.remoteMediaView)
        // Hide the old queueview and put the new video view.
        self.queue_view_controller.view.removeFromSuperview()
        // Now put inside the conversation view.
        self.conversation_view_controller.viewFromVideo = self.remoteMediaView
        self.conversation_view_controller.view.backgroundColor? = UIColor(red:0.98, green:0.84, blue:0.54, alpha:0.6)
        print("My dimensions are width: \(videoTrack.videoDimensions.width) height: \(videoTrack.videoDimensions.height)")
        self.conversation_view_controller.viewFromVideo.frame = conversation_view_controller.view.bounds
        self.conversation_view_controller.viewFromVideo.removeFromSuperview()
        self.conversation_view_controller.view.insertSubview(self.conversation_view_controller.viewFromVideo, belowSubview: self.conversation_view_controller.logo)
        self.view.addSubview(conversation_view_controller.view)
        if (self.conversation_view_controller.state != .Initial) {
            self.conversation_view_controller.activateState(.Initial)
        }
    }
    
    // This is the call that will go back into matching.
    func onTimerExpired(){
        dispatch_async(dispatch_get_main_queue()) {
            if !self.conversation_view_controller.didBothAccepted {
                self.conversation_view_controller.view.removeFromSuperview()
                self.view.addSubview(self.queue_view_controller.view)
                self.hangup()
                // Here comminucate back to the socket so I am put back into the matching queue.
                self.talking_now = false
                let message = ["content": "\(self.client!.identity!)", "type": "back_into_queue"]
                self.sendMessage(message)
            }
        }
    }
}
