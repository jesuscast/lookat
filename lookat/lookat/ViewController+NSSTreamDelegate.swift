
//
//  SocketCommunications.swift
//  lookat
//
//  Created by Macbook on 5/9/16.
//  Copyright © 2016 jesus castaneda. All rights reserved.
//

import Foundation

// MARK: Stream Connection
extension ViewController: NSStreamDelegate {
    
    func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.stringByReplacingOccurrencesOfString("\t", withString: "").stringByReplacingOccurrencesOfString("\0", withString: "").dataUsingEncoding(NSUTF8StringEncoding) {
            do {
                let json = try NSJSONSerialization.JSONObjectWithData(data, options: .MutableContainers) as? [String:AnyObject]
                return json
            } catch {
                print("Something went wrong \(error)")
            }
        }
        return nil
    }
    
    
    func connect() {
        print("connecting...")
        
        var readStream:  Unmanaged<CFReadStream>?
        var writeStream: Unmanaged<CFWriteStream>?
        
        CFStreamCreatePairWithSocketToHost(nil, self.serverAddress, self.serverPort, &readStream, &writeStream)
        
        self.inputStream = readStream!.takeRetainedValue()
        self.outputStream = writeStream!.takeRetainedValue()
        
        self.inputStream.delegate = self
        self.outputStream.delegate = self
        
        self.inputStream.scheduleInRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
        self.outputStream.scheduleInRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
        
        self.inputStream.open()
        self.outputStream.open()
        
        // Now send my uuid in order to get a token
        let message = ["content": UIDevice.currentDevice().identifierForVendor!.UUIDString, "type": "uuid_received"]
        sendMessage(message )
    }
    
    
    func joinChat() {
        // Send the message to join the conversation. Although I don't really need to join it
        // I just need to receive it.
        print("I am joining the chat right now.")
        if let lat_succesful = lat {
            if let long_succesful = long {
                if let name_of_device = UIDevice.currentDevice().identifierForVendor {
                    print(" I am able to send a message")
                    let message = ["content": name_of_device.UUIDString, "type": "join", "lat": "\(lat_succesful)", "long": "\(long_succesful)"]
                    sendMessage(message )
                } else {
                    print("Not able to send a message")
                    let message = ["content": "Justasuperrandomname", "type": "join", "lat": "\(lat_succesful)", "long": "\(long_succesful)"]
                    sendMessage(message )
                }
            } else {
                print("Long has not been set")
            }
        } else {
            print("Lat and Long have not been set")
        }
    }
    
    func try_to_match() {
        let message = ["type": "try_to_match"]
        sendMessage(message )
    }
    func stream(stream: NSStream, handleEvent eventCode: NSStreamEvent) {
        print("stream event\(stream)")
        switch eventCode {
        case NSStreamEvent.OpenCompleted:
            print("Stream opened")
        case NSStreamEvent.HasBytesAvailable:
            if stream == inputStream {
                let bufferSize = 1024
                var buffer = Array<UInt8>(count: bufferSize, repeatedValue: 0)
                
                let bytesRead = inputStream.read(&buffer, maxLength: bufferSize)
                if bytesRead >= 0 {
                    let output = NSString(bytes: &buffer, length: bytesRead, encoding: NSUTF8StringEncoding)
                    let json_unwrapped = convertStringToDictionary(output as! String)
                    if let json = json_unwrapped {
                        if let msg_type = json["type"] {
                            switch msg_type as! String {
                                case "connection_received":
                                    /* Previously I obtained the token until later. With the    new backend I obtain
                                    the token right away after I connect */
                                    if let msg_token = json["token"] {
                                        accessToken = "\(msg_token)"
                                        self.initializeClient()
                                    } else {
                                        print("NO TOKEN RECEIVED.")
                                    }
                                case "ready_to_match":
                                    print("I am out of the matched queue so I am ready to send a new request")
                                    let message = ["content": "Nothing.", "type": "try_to_match"]
                                    sendMessage(message)
                                case "continue_conversation":
                                    print("Both people accepted")
                                    self.conversation_view_controller.didBothAccepted = true
                                    self.conversation_view_controller.onConnectionSet()
                                case "partner_disconnected":
                                    self.talking_now = false
                                    self.conversation_view_controller.didBothAccepted = false
                                    onTimerExpired()
                                case "matched":
                                    print ("I am in and I matched")
                                    if let msg_content = json["content"]{
                                        if let msg_role = json["role"] {
                                            if ("\(msg_role)" == "send_invite") {
                                                invite("\(msg_content)")
                                            } else {
                                                print("Waiting to be invited")
                                            }
                                        // I send an invite only if I am first.
                                        } else {
                                            print("No role in inviting.")
                                        }
                                    }
                                default:
                                    print("I do not know this method \(msg_type)")
                            } // end of switch
                        } //end of if for type
                    } else {
                        // Handle error
                        print("The received data does not have a type")
                    }
                } else {
                    print("Could not unwrapped json")
                }
                
            }
        case NSStreamEvent.ErrorOccurred:
            print("Can not connect to the host!")
        case NSStreamEvent.EndEncountered:
            break
        case NSStreamEvent.EndEncountered:
            print("End encountered")
        case NSStreamEvent.HasSpaceAvailable:
            print("has space available")
        case NSStreamEvent.None:
            print("None?!")
        default:
            print("Unknown event")
        }
    }
    
    func sendMessage(message: [String : String]) {
        print(message)
        print("Sending message")
        do {
            print("Inside do")
            // Transform the array into a json file
            let message_json = try NSJSONSerialization.dataWithJSONObject(message, options: .PrettyPrinted)
            // Parse the json file into a string.
            let message_json_string = NSString(data: message_json,encoding: NSASCIIStringEncoding)
            // Encode the string into streamable data.
            let data: NSData = NSData(data: message_json_string!.dataUsingEncoding(NSASCIIStringEncoding)!)
            // Send the data!
            dispatch_async(dispatch_get_main_queue()) {
                self.outputStream.write(UnsafePointer<UInt8>(data.bytes), maxLength: data.length)
            }
        } catch {
            print(error)
        }
    }
    
    func onTalkPressed(){
        let message = ["content": "Nothing.", "type": "accepted"]
        sendMessage(message)
    }
    func sendVideo(video_id: String) {
        // Send the message to join the conversation. Although I don't really need to join it
        // I just need to receive it.
        if let conversation = defaults.objectForKey("conversation") {
            if let name = defaults.objectForKey("name") {
                let message: [String:String] = ["content": video_id, "conversation": conversation as! String, "type": "video"]
                sendMessage(message)
            }
        }
    }
}