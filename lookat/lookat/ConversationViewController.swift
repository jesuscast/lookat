//
//  ConversationViewController.swift
//  lookat
//
//  Created by Macbook on 5/8/16.
//  Copyright © 2016 jesus castaneda. All rights reserved.
//

import UIKit

// MARK: - Custom Classes
class CircleViewBackground: UIView {
    var circleLayer: CAShapeLayer!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        // self.backgroundColor = UIColor(red:1.00, green:1.00, blue:1.00, alpha:0.4)
        
        // Use UIBezierPath as an easy way to create the CGPath for the layer.
        // The path should be the entire circle.
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: frame.size.width / 2.0, y: frame.size.height / 2.0), radius: (frame.size.width - 14)/2, startAngle: CGFloat(M_PI * 0), endAngle: CGFloat(M_PI * 2.0), clockwise: true)
        
        // Setup the CAShapeLayer with the path, colors, and line width
        circleLayer = CAShapeLayer()
        circleLayer.path = circlePath.CGPath
        circleLayer.fillColor = UIColor.clearColor().CGColor
        circleLayer.strokeColor = UIColor(red:0.20, green:0.60, blue:0.86, alpha:1.0).CGColor
        circleLayer.lineWidth = 6.0;
        
        // Don't draw the circle initially
        circleLayer.strokeEnd = 1.0
        
        layer.addSublayer(circleLayer)
    }
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class CircleView: UIView {
    var circleLayer: CAShapeLayer!
    var timer = NSTimer()
    var cancellingCallback: (Void -> Void)? = nil
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clearColor()
        
        // Use UIBezierPath as an easy way to create the CGPath for the layer.
        // The path should be the entire circle.
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: frame.size.width / 2.0, y: frame.size.height / 2.0), radius: (frame.size.width - 14)/2, startAngle: CGFloat(M_PI * 1.5), endAngle: CGFloat(M_PI * 3.5), clockwise: true)
        
        // Setup the CAShapeLayer with the path, colors, and line width
        circleLayer = CAShapeLayer()
        circleLayer.path = circlePath.CGPath
        circleLayer.fillColor = UIColor.clearColor().CGColor
        // Green
        circleLayer.strokeColor = UIColor(red:0.18, green:0.80, blue:0.44, alpha:1.0).CGColor
        circleLayer.lineWidth = 6.0;
        
        // Don't draw the circle initially
        circleLayer.strokeEnd = 0.0
        
        layer.addSublayer(circleLayer)
    }
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func animateCircle(duration: NSTimeInterval) {
        // We want to animate the strokeEnd property of the circleLayer
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        
        // Set the animation duration appropriately
        animation.duration = duration
        
        // Animate from 0 (no circle) to 1 (full circle)
        animation.fromValue = 0
        animation.toValue = 1
        
        // Do a linear animation (i.e. the speed of the animation stays the same)
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        
        // Set the circleLayer's strokeEnd property to 1.0 now so that it's the
        // right value when the animation ends.
        circleLayer.strokeEnd = 1.0
        // Do the actual animation
        circleLayer.addAnimation(animation, forKey: "animateCircle")
        // Schedule the killing of everything and going back into matching.
        timer = NSTimer.scheduledTimerWithTimeInterval(11.0, target: self, selector: #selector(goBackToMatching), userInfo: nil, repeats: false)
    }
    
    func goBackToMatching(){
        // self
        cancellingCallback?()
        print("I am supposed to back into matching right now")
    }
}
// MARK: - States
enum InteractionState: Equatable {
    case Initial
    case YouAccepted
    case HeAccepted
    case Conversating
    case BackToMatching
    case Neutral
}

func ==(a: InteractionState, b:InteractionState) -> Bool {
    switch(a,b) {
    case(.Initial, .Initial): return true
    case(.Neutral, .Neutral): return true
    case(.YouAccepted, .YouAccepted): return true
    case (.HeAccepted, .HeAccepted): return true
    case (.Conversating, .Conversating): return true
    default: return false
    }
}

class ConversationViewController: UIViewController {
    
    // MARK: - Properties
    
    var state: InteractionState = .Neutral
    var didSetupConstraints = false
    var btnGreen = UIButton()
    var logo = UIButton()
    var viewFromVideo: UIView = UIView()
    var btnEnd = UIButton()
    var circleView : CircleView? = CircleView()
    var circleViewBackground : CircleViewBackground? = CircleViewBackground()
    var screenRect = CGRect()
    var screenWidth = CGFloat(0.0)
    var screenHeight = CGFloat(0.0)
    var matchingLabel = UILabel()
    var onTalkPressed: (Void -> Void)? = nil
    var onGreenPressed: (Void -> Void)? = nil
    var onClosePressed: (Void -> Void)? = nil
    // Called after the video is playing.
    var onStreamStarted: (Void -> Void)? = nil
    // Called if we encounter an error.
    var onError: (String -> Void)? = nil
    // Call this whenever the timer expires
    var onTimerExpired: (Void -> Void)? = nil
    var didBothAccepted = false
    // MARK: - Lifecycle
    override func loadView() {
        view = UIView()
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(viewFromVideo)
        self.view.addSubview(logo)
        self.view.addSubview(btnGreen)
        self.view.addSubview(btnEnd)
        self.view.backgroundColor? = UIColor(red:0.98, green:0.84, blue:0.54, alpha:1.0)
        UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: false)
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        circleView?.cancellingCallback = overridingBackToMatching
        circleView?.animateCircle(10.0)
        // Here update the view or whatever.
    }
    
    func overridingBackToMatching(){
        print("I do not even know if this is possible")
        onTimerExpired?()
    }
    override func viewWillAppear(animated: Bool) {
        btnEnd.frame = CGRectMake(-100, -60, (54/2.0), (57/2.0))
    }
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    override func  preferredStatusBarStyle()-> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
    
    override func shouldAutorotate() -> Bool {
        return false
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.Portrait
    }
    func imageResize (image:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.drawInRect(CGRect(origin: CGPointZero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage
    }
    
    // MARK: - State manager
    func activateState(stateGoal: InteractionState) {
        switch(stateGoal) {
        case .Initial:
            screenRect = UIScreen.mainScreen().bounds
            screenWidth = screenRect.size.width
            screenHeight = screenRect.size.height
            
            // Coordinates for the btn Green
            let wG = screenWidth*0.3
            let hG = wG
            let xG = (screenWidth-wG) / 2.0
            let yG = screenHeight-hG-CGFloat(20)
            
            // Coordinates for the loader
            let wLoader = wG+20
            let hLoader = wLoader
            let xLoader = (screenWidth-wLoader) / 2.0
            let yLoader = (yG-(hLoader-hG)/2.0)
            // Set the backgrounds
            btnGreen.setBackgroundImage(UIImage(named: "Yes.png"), forState: UIControlState.Normal)
            //logo.setBackgroundImage(UIImage(named: "top-left.png"), forState: UIControlState.Normal)
            btnGreen.setBackgroundImage(UIImage(named: "yes-clicked.png"), forState: UIControlState.Highlighted)
            let size = CGSizeMake(100, 100)
            let bg = UIImage(named: "new_x.png")
            let bg2 = imageResize(bg!, sizeChange: size)
            btnEnd.setImage(bg2, forState: .Normal)
            // set targets
            btnGreen.addTarget(self, action: "accept:", forControlEvents: .TouchUpInside)
            btnEnd.addTarget(self, action: "close:", forControlEvents: .TouchUpInside)
            // set all the frames
            btnGreen.frame = CGRect(x: xG, y: yG, width: wG, height: hG)
            logo.frame = CGRect(x: 20, y: 30, width: 74, height: 31)
            logo.adjustsImageWhenDisabled = false
            logo.adjustsImageWhenHighlighted = false
            //            btnEnd.frame = CGRectMake(-102, 22, 40.0, 35.0)
            btnEnd.frame = CGRect(x: -102.0, y: 22.0, width: 80.0, height: 40.0)
            // btnEnd.imageView!.contentMode = .ScaleToFill
            btnEnd.contentMode = UIViewContentMode.ScaleToFill
            //
            btnEnd.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Fill
            btnEnd.contentVerticalAlignment = UIControlContentVerticalAlignment.Fill
            btnEnd.imageView?.contentMode = UIViewContentMode.ScaleAspectFit
            //
            // Update the circle view
            if let circleViewTmp = circleView {
                circleViewTmp.removeFromSuperview()
                circleView = nil
            }
            if let circleViewBgTmp = circleViewBackground {
                circleViewBgTmp.removeFromSuperview()
                circleViewBackground = nil
            }
            circleView = CircleView(frame: CGRect(x: xLoader, y: yLoader, width: wLoader, height: hLoader))
            self.view.insertSubview(circleView!, belowSubview: self.btnGreen)
            circleViewBackground = CircleViewBackground(frame: CGRect(x: xLoader, y: yLoader, width: wLoader, height: hLoader))
            self.view.insertSubview(circleViewBackground!, belowSubview: self.circleView!)
            // circleView.animateCircle(10.0)
            state = .Initial
        case .HeAccepted:
            state = .HeAccepted
        case .YouAccepted:
            state = .YouAccepted
        case .Conversating:
            let xE = self.screenWidth-self.btnEnd.frame.size.width-22
            let yE = self.logo.frame.origin.y + 2
            UIView.animateWithDuration(0.5, delay: 0.0, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
                // move the loader and the circle out of the visible space
                self.btnGreen.frame.origin.y = self.btnGreen.frame.origin.y + 500
                self.circleView!.frame.origin.y = self.circleView!.frame.origin.y + 500
                self.circleViewBackground!.frame.origin.y = self.circleViewBackground!.frame.origin.y + 500
                self.btnEnd.frame.origin.x = xE
                self.btnEnd.frame.origin.y = yE
                }, completion: nil)
            state = .Conversating
        case .BackToMatching:
            state = .BackToMatching
            overridingBackToMatching()
        case .Neutral:
            state = .Neutral
        }
    }
    // MARK: - OTSubscriber delegate callbacks
    
    // Accept the other person
    func accept(sender: UIButton) {
        if(state==InteractionState.Initial) {
            activateState(.YouAccepted)
        }
        onGreenPressed?()
        btnGreen.setBackgroundImage(UIImage(named: "yes-clicked.png"), forState: UIControlState.Normal)
        btnGreen.removeFromSuperview()
        self.view.insertSubview(btnGreen, aboveSubview: circleView!)
        onTalkPressed?()
    }
    // Set up the connection
    func onConnectionSet() {
        activateState(.Conversating)
    }
    // Reject the other person
    func reject(sender: UIButton) {
        if(state==InteractionState.Initial) {
            activateState(.BackToMatching)
        }
    }
    // Close the connection
    func close(sender: UIButton) {
        self.didBothAccepted = false
        if(state==InteractionState.Conversating) {
            NSLog("connection closed by the user")
            activateState(.BackToMatching)
        }
    }
}